# Tap

Tap is a python3 based 'touch' alternative.
<br>
It has 2 flags:
<br>
"--help" - displays the program's help menu
<br>
"-u"     - updates the time of the provided file if it exists.
<br>
That's all there is to it!
