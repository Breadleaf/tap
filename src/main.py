from datetime import datetime as dt
from sys import argv
from os import utime

def display_help_menu(exit_code=""):
	print("Usage: tap [OPTION]... [FILE]... ")
	print("Update the access and modification times the provided [FILE] to the current time.")
	print("\nA [FILE] argument that does not exist is created empty, unless '-u' flag is used.")
	print("\nAvailable flags:")
	print("    --help     Display this help menu.")
	print("    -u         Update the time of the provided file if it exists.")
	if exit_code != "":
		exit(exit_code)

def arg_parser(_list):
	return_list = []
	skip = False

	for idx, el in enumerate(_list):
		if skip:
			skip = False
			continue

		# Process -u flag edge cases
		if el == "-u":

			# Make sure that a -f flag is not followed by another flag
			if _list[idx+1] == "--help" or _list[idx+1] == "-u":
				print("Flag error: '-u' flag cannot be followed by another flag!")
				exit(2)

			skip = True
			return_list.append([el, _list[idx+1]])
			continue

		return_list.append([el])
	return return_list

def main(list_arg_list):
	make_file = lambda name : open(name, "a+")
	update_time = lambda name : utime(name, (dt.now().timestamp(), dt.now().timestamp()))

	for arg_list in list_arg_list:
		# Display help menu
		if arg_list[0] == "--help" and len(arg_list) == 1:
			display_help_menu()

		# Update time (no create file)
		elif arg_list[0] == "-u" and len(arg_list) == 2:
			try:
				update_time(arg_list[1])
			except FileNotFoundError:
				print(f"'{arg_list[1]}' not found!")
				exit(4)

		# False -u check
		elif arg_list[0] == "-u" and len(arg_list) == 1:
			print("Flag error: '-u' flag takes at least 1 argument!")
			exit(3)

		# Create and/or update time of a given file.
		else:
			make_file(arg_list[0])
			update_time(arg_list[0])

if __name__ == "__main__":
	if len(argv) < 2:
		print("tap: missing file operand")
		print("Try 'tap --help' for more information.")
		exit(1)
	main(arg_parser(argv[1:]))
